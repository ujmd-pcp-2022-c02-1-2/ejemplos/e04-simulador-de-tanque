﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TANQUE
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnLLENAR_Click(object sender, EventArgs e)
        {
            tmrLLENADO.Start();
            tmrVACIADO.Stop();
            panelLEDLLENADO.BackColor=Color.GreenYellow;
            panelLEDVACIADO.BackColor = Color.FromArgb(0, 32, 0);
        }

        private void btnVACIAR_Click(object sender, EventArgs e)
        {
            tmrVACIADO.Start();
            tmrLLENADO.Stop();
            panelLEDVACIADO.BackColor = Color.GreenYellow;
            panelLEDLLENADO.BackColor = Color.FromArgb(0, 32, 0);
        }

        private void tmrLLENADO_Tick(object sender, EventArgs e)
        {
            if (panelcerveza.Height < paneltanque.Height)
            {
                panelcerveza.Height++;
            }
            else
            {
                tmrLLENADO.Stop();
                panelLEDLLENADO.BackColor = Color.FromArgb(0, 32, 0);
            }            
        }

        private void tmrVACIADO_Tick(object sender, EventArgs e)
        {
            if (panelcerveza.Height>0)
            {
                panelcerveza.Height--;
            }
            else
            {
                tmrVACIADO.Enabled=false;
                panelLEDVACIADO.BackColor=Color.FromArgb(0,32,0);
            }

            
        }

        private void btnSTOP_Click(object sender, EventArgs e)
        {
            tmrLLENADO.Stop();
            tmrVACIADO.Stop();

            panelLEDLLENADO.BackColor = Color.FromArgb(0,32,0);
            panelLEDVACIADO.BackColor = Color.FromArgb(0,32,0);
        }
    }
}
