﻿namespace TANQUE
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelimagen = new System.Windows.Forms.Panel();
            this.paneltanque = new System.Windows.Forms.Panel();
            this.panelcerveza = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSTOP = new System.Windows.Forms.Button();
            this.panelLEDVACIADO = new System.Windows.Forms.Panel();
            this.panelLEDLLENADO = new System.Windows.Forms.Panel();
            this.btnLLENAR = new System.Windows.Forms.Button();
            this.btnVACIAR = new System.Windows.Forms.Button();
            this.tmrLLENADO = new System.Windows.Forms.Timer(this.components);
            this.tmrVACIADO = new System.Windows.Forms.Timer(this.components);
            this.panelimagen.SuspendLayout();
            this.paneltanque.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelimagen
            // 
            this.panelimagen.BackColor = System.Drawing.Color.Khaki;
            this.panelimagen.BackgroundImage = global::TANQUE.Properties.Resources.Recurso_3480;
            this.panelimagen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelimagen.Controls.Add(this.paneltanque);
            this.panelimagen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelimagen.Location = new System.Drawing.Point(203, 3);
            this.panelimagen.Name = "panelimagen";
            this.panelimagen.Size = new System.Drawing.Size(391, 377);
            this.panelimagen.TabIndex = 1;
            // 
            // paneltanque
            // 
            this.paneltanque.BackColor = System.Drawing.Color.Black;
            this.paneltanque.Controls.Add(this.panelcerveza);
            this.paneltanque.Location = new System.Drawing.Point(215, 119);
            this.paneltanque.Name = "paneltanque";
            this.paneltanque.Size = new System.Drawing.Size(12, 143);
            this.paneltanque.TabIndex = 2;
            // 
            // panelcerveza
            // 
            this.panelcerveza.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(168)))), ((int)(((byte)(55)))));
            this.panelcerveza.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelcerveza.Location = new System.Drawing.Point(0, 111);
            this.panelcerveza.Name = "panelcerveza";
            this.panelcerveza.Size = new System.Drawing.Size(12, 32);
            this.panelcerveza.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 323F));
            this.tableLayoutPanel1.Controls.Add(this.panelimagen, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(47, 27);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(597, 383);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.btnSTOP);
            this.panel1.Controls.Add(this.panelLEDVACIADO);
            this.panel1.Controls.Add(this.panelLEDLLENADO);
            this.panel1.Controls.Add(this.btnLLENAR);
            this.panel1.Controls.Add(this.btnVACIAR);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 377);
            this.panel1.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(23, 179);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(134, 164);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Altura";
            // 
            // btnSTOP
            // 
            this.btnSTOP.Location = new System.Drawing.Point(23, 139);
            this.btnSTOP.Name = "btnSTOP";
            this.btnSTOP.Size = new System.Drawing.Size(86, 34);
            this.btnSTOP.TabIndex = 5;
            this.btnSTOP.Text = "STOP";
            this.btnSTOP.UseVisualStyleBackColor = true;
            this.btnSTOP.Click += new System.EventHandler(this.btnSTOP_Click);
            // 
            // panelLEDVACIADO
            // 
            this.panelLEDVACIADO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panelLEDVACIADO.Location = new System.Drawing.Point(115, 90);
            this.panelLEDVACIADO.Name = "panelLEDVACIADO";
            this.panelLEDVACIADO.Size = new System.Drawing.Size(41, 34);
            this.panelLEDVACIADO.TabIndex = 4;
            // 
            // panelLEDLLENADO
            // 
            this.panelLEDLLENADO.BackColor = System.Drawing.Color.Green;
            this.panelLEDLLENADO.Location = new System.Drawing.Point(115, 39);
            this.panelLEDLLENADO.Name = "panelLEDLLENADO";
            this.panelLEDLLENADO.Size = new System.Drawing.Size(41, 30);
            this.panelLEDLLENADO.TabIndex = 3;
            // 
            // btnLLENAR
            // 
            this.btnLLENAR.Location = new System.Drawing.Point(23, 35);
            this.btnLLENAR.Name = "btnLLENAR";
            this.btnLLENAR.Size = new System.Drawing.Size(86, 34);
            this.btnLLENAR.TabIndex = 2;
            this.btnLLENAR.Text = "LLENAR";
            this.btnLLENAR.UseVisualStyleBackColor = true;
            this.btnLLENAR.Click += new System.EventHandler(this.btnLLENAR_Click);
            // 
            // btnVACIAR
            // 
            this.btnVACIAR.Location = new System.Drawing.Point(23, 90);
            this.btnVACIAR.Name = "btnVACIAR";
            this.btnVACIAR.Size = new System.Drawing.Size(86, 34);
            this.btnVACIAR.TabIndex = 1;
            this.btnVACIAR.Text = "VACIAR";
            this.btnVACIAR.UseVisualStyleBackColor = true;
            this.btnVACIAR.Click += new System.EventHandler(this.btnVACIAR_Click);
            // 
            // tmrLLENADO
            // 
            this.tmrLLENADO.Interval = 25;
            this.tmrLLENADO.Tick += new System.EventHandler(this.tmrLLENADO_Tick);
            // 
            // tmrVACIADO
            // 
            this.tmrVACIADO.Interval = 25;
            this.tmrVACIADO.Tick += new System.EventHandler(this.tmrVACIADO_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 410);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panelimagen.ResumeLayout(false);
            this.paneltanque.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelimagen;
        private System.Windows.Forms.Panel paneltanque;
        private System.Windows.Forms.Panel panelcerveza;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnVACIAR;
        private System.Windows.Forms.Timer tmrLLENADO;
        private System.Windows.Forms.Button btnLLENAR;
        private System.Windows.Forms.Timer tmrVACIADO;
        private System.Windows.Forms.Panel panelLEDVACIADO;
        private System.Windows.Forms.Panel panelLEDLLENADO;
        private System.Windows.Forms.Button btnSTOP;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
    }
}

