﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TANQUE
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnLLENAR_Click(object sender, EventArgs e)
        {
            tmrLLENADO.Start();
            tmrVACIADO.Stop();
            panelLEDLLENADO.BackColor=Color.GreenYellow;
            panelLEDVACIADO.BackColor = Color.FromArgb(0, 32, 0);
        }

        private void btnVACIAR_Click(object sender, EventArgs e)
        {
            tmrVACIADO.Start();
            tmrLLENADO.Stop();
            panelLEDVACIADO.BackColor = Color.GreenYellow;
            panelLEDLLENADO.BackColor = Color.FromArgb(0, 32, 0);
        }

        private void tmrLLENADO_Tick(object sender, EventArgs e)
        {
            if (panelcerveza.Height < paneltanque.Height)
            {
                panelcerveza.Height++;
                double Nivel = (double)nudaltura.Value * (double)panelcerveza.Height / paneltanque.Height;
                labelnivel.Text = Nivel.ToString("0.00") + (char)252 + '\u263A';
                double Volumen = 0;
                Volumen = 3.1416 * Math.Pow((double)nuddiametro.Value, 2) * Nivel / 2;
                lblvolumen.Text = Volumen.ToString("0.0000");
                lbllitros.Text = (Volumen * 1000).ToString("0.00");
            }
            else
            {
                tmrLLENADO.Stop();
                panelLEDLLENADO.BackColor = Color.FromArgb(0, 32, 0);
            }            
        }

        private void tmrVACIADO_Tick(object sender, EventArgs e)
        {
            if (panelcerveza.Height>0)
            {
                panelcerveza.Height--;
                double Nivel = (double)nudaltura.Value * (double)panelcerveza.Height / paneltanque.Height;
                labelnivel.Text = Nivel.ToString("0.00");
                double Volumen = 3.1416 * Math.Pow((double)nuddiametro.Value, 2) * Nivel / 2;
                lblvolumen.Text = Volumen.ToString("0.0000");
                lbllitros.Text = (Volumen * 1000).ToString("0.00");
            }
            else
            {
                tmrVACIADO.Enabled=false;
                panelLEDVACIADO.BackColor=Color.FromArgb(0,32,0);
            }

            
        }

        private void btnSTOP_Click(object sender, EventArgs e)
        {
            tmrLLENADO.Stop();
            tmrVACIADO.Stop();

            panelLEDLLENADO.BackColor = Color.FromArgb(0,32,0);
            panelLEDVACIADO.BackColor = Color.FromArgb(0,32,0);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            panelcerveza.Height = 0;
        }
    }
}
