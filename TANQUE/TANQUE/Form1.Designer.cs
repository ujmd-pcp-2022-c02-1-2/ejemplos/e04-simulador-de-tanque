﻿namespace TANQUE
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panelimagen = new System.Windows.Forms.Panel();
            this.paneltanque = new System.Windows.Forms.Panel();
            this.panelcerveza = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lbllitros = new System.Windows.Forms.Label();
            this.labelvolumenl = new System.Windows.Forms.Label();
            this.lblvolumen = new System.Windows.Forms.Label();
            this.labelvolumen = new System.Windows.Forms.Label();
            this.nuddiametro = new System.Windows.Forms.NumericUpDown();
            this.lbldiametro = new System.Windows.Forms.Label();
            this.lblnivel = new System.Windows.Forms.Label();
            this.lblaltura = new System.Windows.Forms.Label();
            this.nudaltura = new System.Windows.Forms.NumericUpDown();
            this.labelnivel = new System.Windows.Forms.Label();
            this.btnSTOP = new System.Windows.Forms.Button();
            this.panelLEDVACIADO = new System.Windows.Forms.Panel();
            this.panelLEDLLENADO = new System.Windows.Forms.Panel();
            this.btnLLENAR = new System.Windows.Forms.Button();
            this.btnVACIAR = new System.Windows.Forms.Button();
            this.tmrLLENADO = new System.Windows.Forms.Timer(this.components);
            this.tmrVACIADO = new System.Windows.Forms.Timer(this.components);
            this.panelimagen.SuspendLayout();
            this.paneltanque.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuddiametro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudaltura)).BeginInit();
            this.SuspendLayout();
            // 
            // panelimagen
            // 
            this.panelimagen.BackColor = System.Drawing.Color.Khaki;
            this.panelimagen.BackgroundImage = global::TANQUE.Properties.Resources.Recurso_3480;
            this.panelimagen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelimagen.Controls.Add(this.paneltanque);
            this.panelimagen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelimagen.Location = new System.Drawing.Point(303, 3);
            this.panelimagen.Name = "panelimagen";
            this.panelimagen.Size = new System.Drawing.Size(416, 435);
            this.panelimagen.TabIndex = 1;
            // 
            // paneltanque
            // 
            this.paneltanque.BackColor = System.Drawing.Color.Black;
            this.paneltanque.Controls.Add(this.panelcerveza);
            this.paneltanque.Location = new System.Drawing.Point(215, 119);
            this.paneltanque.Name = "paneltanque";
            this.paneltanque.Size = new System.Drawing.Size(12, 143);
            this.paneltanque.TabIndex = 2;
            // 
            // panelcerveza
            // 
            this.panelcerveza.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(205)))), ((int)(((byte)(168)))), ((int)(((byte)(55)))));
            this.panelcerveza.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelcerveza.Location = new System.Drawing.Point(0, 111);
            this.panelcerveza.Name = "panelcerveza";
            this.panelcerveza.Size = new System.Drawing.Size(12, 32);
            this.panelcerveza.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 323F));
            this.tableLayoutPanel1.Controls.Add(this.panelimagen, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(24, 27);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(722, 441);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.btnSTOP);
            this.panel1.Controls.Add(this.panelLEDVACIADO);
            this.panel1.Controls.Add(this.panelLEDLLENADO);
            this.panel1.Controls.Add(this.btnLLENAR);
            this.panel1.Controls.Add(this.btnVACIAR);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(294, 435);
            this.panel1.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.lbllitros, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.labelvolumenl, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.lblvolumen, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.labelvolumen, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.nuddiametro, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.lbldiametro, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblnivel, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblaltura, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.nudaltura, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelnivel, 1, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 153);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(288, 251);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // lbllitros
            // 
            this.lbllitros.AutoSize = true;
            this.lbllitros.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllitros.Font = new System.Drawing.Font("Arial", 9F);
            this.lbllitros.Location = new System.Drawing.Point(147, 120);
            this.lbllitros.Name = "lbllitros";
            this.lbllitros.Size = new System.Drawing.Size(138, 30);
            this.lbllitros.TabIndex = 9;
            this.lbllitros.Text = "0.00";
            this.lbllitros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelvolumenl
            // 
            this.labelvolumenl.AutoSize = true;
            this.labelvolumenl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelvolumenl.Font = new System.Drawing.Font("Arial", 9F);
            this.labelvolumenl.Location = new System.Drawing.Point(3, 120);
            this.labelvolumenl.Name = "labelvolumenl";
            this.labelvolumenl.Size = new System.Drawing.Size(138, 30);
            this.labelvolumenl.TabIndex = 8;
            this.labelvolumenl.Text = "Volumen (L):";
            // 
            // lblvolumen
            // 
            this.lblvolumen.AutoSize = true;
            this.lblvolumen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblvolumen.Font = new System.Drawing.Font("Arial", 9F);
            this.lblvolumen.Location = new System.Drawing.Point(147, 90);
            this.lblvolumen.Name = "lblvolumen";
            this.lblvolumen.Size = new System.Drawing.Size(138, 30);
            this.lblvolumen.TabIndex = 7;
            this.lblvolumen.Text = "0.00";
            this.lblvolumen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelvolumen
            // 
            this.labelvolumen.AutoSize = true;
            this.labelvolumen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelvolumen.Font = new System.Drawing.Font("Arial", 9F);
            this.labelvolumen.Location = new System.Drawing.Point(3, 90);
            this.labelvolumen.Name = "labelvolumen";
            this.labelvolumen.Size = new System.Drawing.Size(138, 30);
            this.labelvolumen.TabIndex = 6;
            this.labelvolumen.Text = "Volumen (m³):";
            // 
            // nuddiametro
            // 
            this.nuddiametro.DecimalPlaces = 2;
            this.nuddiametro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nuddiametro.Font = new System.Drawing.Font("Arial", 9F);
            this.nuddiametro.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nuddiametro.Location = new System.Drawing.Point(147, 63);
            this.nuddiametro.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nuddiametro.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nuddiametro.Name = "nuddiametro";
            this.nuddiametro.Size = new System.Drawing.Size(138, 28);
            this.nuddiametro.TabIndex = 5;
            this.nuddiametro.Value = new decimal(new int[] {
            12,
            0,
            0,
            65536});
            // 
            // lbldiametro
            // 
            this.lbldiametro.AutoSize = true;
            this.lbldiametro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbldiametro.Font = new System.Drawing.Font("Arial", 9F);
            this.lbldiametro.Location = new System.Drawing.Point(3, 60);
            this.lbldiametro.Name = "lbldiametro";
            this.lbldiametro.Size = new System.Drawing.Size(138, 30);
            this.lbldiametro.TabIndex = 4;
            this.lbldiametro.Text = "Diámetro (m):";
            // 
            // lblnivel
            // 
            this.lblnivel.AutoSize = true;
            this.lblnivel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblnivel.Font = new System.Drawing.Font("Arial", 9F);
            this.lblnivel.Location = new System.Drawing.Point(3, 30);
            this.lblnivel.Name = "lblnivel";
            this.lblnivel.Size = new System.Drawing.Size(138, 30);
            this.lblnivel.TabIndex = 2;
            this.lblnivel.Text = "Nivel (m):";
            // 
            // lblaltura
            // 
            this.lblaltura.AutoSize = true;
            this.lblaltura.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblaltura.Font = new System.Drawing.Font("Arial", 9F);
            this.lblaltura.Location = new System.Drawing.Point(3, 0);
            this.lblaltura.Name = "lblaltura";
            this.lblaltura.Size = new System.Drawing.Size(138, 30);
            this.lblaltura.TabIndex = 0;
            this.lblaltura.Text = "Altura (m):";
            this.lblaltura.Click += new System.EventHandler(this.label1_Click);
            // 
            // nudaltura
            // 
            this.nudaltura.DecimalPlaces = 2;
            this.nudaltura.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nudaltura.Font = new System.Drawing.Font("Arial", 9F);
            this.nudaltura.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudaltura.Location = new System.Drawing.Point(147, 3);
            this.nudaltura.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudaltura.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudaltura.Name = "nudaltura";
            this.nudaltura.Size = new System.Drawing.Size(138, 28);
            this.nudaltura.TabIndex = 1;
            this.nudaltura.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // labelnivel
            // 
            this.labelnivel.AutoSize = true;
            this.labelnivel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelnivel.Font = new System.Drawing.Font("Arial", 9F);
            this.labelnivel.Location = new System.Drawing.Point(147, 30);
            this.labelnivel.Name = "labelnivel";
            this.labelnivel.Size = new System.Drawing.Size(138, 30);
            this.labelnivel.TabIndex = 3;
            this.labelnivel.Text = "0.00";
            this.labelnivel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSTOP
            // 
            this.btnSTOP.Location = new System.Drawing.Point(70, 92);
            this.btnSTOP.Name = "btnSTOP";
            this.btnSTOP.Size = new System.Drawing.Size(86, 34);
            this.btnSTOP.TabIndex = 5;
            this.btnSTOP.Text = "STOP";
            this.btnSTOP.UseVisualStyleBackColor = true;
            this.btnSTOP.Click += new System.EventHandler(this.btnSTOP_Click);
            // 
            // panelLEDVACIADO
            // 
            this.panelLEDVACIADO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panelLEDVACIADO.Location = new System.Drawing.Point(115, 52);
            this.panelLEDVACIADO.Name = "panelLEDVACIADO";
            this.panelLEDVACIADO.Size = new System.Drawing.Size(41, 34);
            this.panelLEDVACIADO.TabIndex = 4;
            // 
            // panelLEDLLENADO
            // 
            this.panelLEDLLENADO.BackColor = System.Drawing.Color.Green;
            this.panelLEDLLENADO.Location = new System.Drawing.Point(115, 7);
            this.panelLEDLLENADO.Name = "panelLEDLLENADO";
            this.panelLEDLLENADO.Size = new System.Drawing.Size(41, 30);
            this.panelLEDLLENADO.TabIndex = 3;
            // 
            // btnLLENAR
            // 
            this.btnLLENAR.Location = new System.Drawing.Point(23, 3);
            this.btnLLENAR.Name = "btnLLENAR";
            this.btnLLENAR.Size = new System.Drawing.Size(86, 34);
            this.btnLLENAR.TabIndex = 2;
            this.btnLLENAR.Text = "LLENAR";
            this.btnLLENAR.UseVisualStyleBackColor = true;
            this.btnLLENAR.Click += new System.EventHandler(this.btnLLENAR_Click);
            // 
            // btnVACIAR
            // 
            this.btnVACIAR.Location = new System.Drawing.Point(23, 52);
            this.btnVACIAR.Name = "btnVACIAR";
            this.btnVACIAR.Size = new System.Drawing.Size(86, 34);
            this.btnVACIAR.TabIndex = 1;
            this.btnVACIAR.Text = "VACIAR";
            this.btnVACIAR.UseVisualStyleBackColor = true;
            this.btnVACIAR.Click += new System.EventHandler(this.btnVACIAR_Click);
            // 
            // tmrLLENADO
            // 
            this.tmrLLENADO.Interval = 75;
            this.tmrLLENADO.Tick += new System.EventHandler(this.tmrLLENADO_Tick);
            // 
            // tmrVACIADO
            // 
            this.tmrVACIADO.Interval = 75;
            this.tmrVACIADO.Tick += new System.EventHandler(this.tmrVACIADO_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 546);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Tanque";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelimagen.ResumeLayout(false);
            this.paneltanque.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuddiametro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudaltura)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelimagen;
        private System.Windows.Forms.Panel paneltanque;
        private System.Windows.Forms.Panel panelcerveza;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnVACIAR;
        private System.Windows.Forms.Timer tmrLLENADO;
        private System.Windows.Forms.Button btnLLENAR;
        private System.Windows.Forms.Timer tmrVACIADO;
        private System.Windows.Forms.Panel panelLEDVACIADO;
        private System.Windows.Forms.Panel panelLEDLLENADO;
        private System.Windows.Forms.Button btnSTOP;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblaltura;
        private System.Windows.Forms.Label lbllitros;
        private System.Windows.Forms.Label labelvolumenl;
        private System.Windows.Forms.Label lblvolumen;
        private System.Windows.Forms.Label labelvolumen;
        private System.Windows.Forms.NumericUpDown nuddiametro;
        private System.Windows.Forms.Label lbldiametro;
        private System.Windows.Forms.Label lblnivel;
        private System.Windows.Forms.NumericUpDown nudaltura;
        private System.Windows.Forms.Label labelnivel;
    }
}

