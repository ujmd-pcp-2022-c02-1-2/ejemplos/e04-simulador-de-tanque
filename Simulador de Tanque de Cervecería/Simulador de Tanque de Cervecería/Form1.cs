﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulador_de_Tanque_de_Cervecería
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnLlenar_Click(object sender, EventArgs e)
        {
            // Encendemos el Timer de Llenado y apagamos el otro
            tmrLlenado.Start();
            tmrVaciado.Stop();
        }

        private void btnVaciar_Click(object sender, EventArgs e)
        {
            // Encendemos el Timer de Vaciado y apagamos el otro
            tmrLlenado.Stop();
            tmrVaciado.Start();
        }

        private void tmrLlenado_Tick(object sender, EventArgs e)
        {
            if (panelCerveza.Height < PanelInteriorTanque.Height)
            {
                panelCerveza.Height++;
            }
            else
            {
                tmrLlenado.Stop();
            }
        }

        private void tmrVaciado_Tick(object sender, EventArgs e)
        {
            if (panelCerveza.Height > 0)
            {
                panelCerveza.Height--;
            }
            else
            {
                tmrVaciado.Stop();
            }
        }

        private void btnParo_Click(object sender, EventArgs e)
        {
            // Apago ambos timers para que no llene ni vacíe
            tmrLlenado.Stop();
            tmrVaciado.Stop();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            panelCerveza.Height = 0;
        }

        private void tmrEstado_Tick(object sender, EventArgs e)
        {
            if (tmrLlenado.Enabled)
            {
                chkValvulaLlenado.Checked = true;
            }
            else
            {
                chkValvulaLlenado.Checked = false;
            }

            if (tmrVaciado.Enabled)
            {
                chkValvulaVaciado.Checked = true;
            }
            else
            {
                chkValvulaVaciado.Checked = false;
            }

            if (panelCerveza.Height == 0)
            {
                chkTanqueVacío.Checked = true;
            }
            else
            {
                chkTanqueVacío.Checked = false;
            }

            if (panelCerveza.Height >= PanelInteriorTanque.Height)
            {
                chkTanqueLleno.Checked = true;
            }
            else
            {
                chkTanqueLleno.Checked = false;
            }

            // Cálculos geométricos
            // Regla de tres para proporción de altura
            // (nudAltura) hMaxTanque --> PanelInteriorTanque.Height
            //              hCerveza   --> panelCerveza.Height

            double hCerveza = (double)nudAltura.Value * 
                panelCerveza.Height / PanelInteriorTanque.Height;

            lblNivel.Text = hCerveza.ToString("0.0000");

            // Volumen (m³) = Pi * r^2 *hCerveza;
            double radio = (double)nudDiámetro.Value / 2;
            double Volumen = Math.PI * Math.Pow(radio, 2) * hCerveza;

            lblVolumen.Text = Volumen.ToString("0.0000");

            double Litros = Volumen * 1000;
            lblLitros.Text = Litros.ToString("0.00");
        }
    }
}
