﻿namespace Simulador_de_Tanque_de_Cervecería
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.PanelImagen = new System.Windows.Forms.Panel();
            this.PanelInteriorTanque = new System.Windows.Forms.Panel();
            this.panelCerveza = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nudAltura = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNivel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nudDiámetro = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.lblVolumen = new System.Windows.Forms.Label();
            this.lblLitros = new System.Windows.Forms.Label();
            this.btnParo = new System.Windows.Forms.Button();
            this.btnVaciar = new System.Windows.Forms.Button();
            this.btnLlenar = new System.Windows.Forms.Button();
            this.tmrLlenado = new System.Windows.Forms.Timer(this.components);
            this.tmrVaciado = new System.Windows.Forms.Timer(this.components);
            this.tmrEstado = new System.Windows.Forms.Timer(this.components);
            this.chkValvulaLlenado = new System.Windows.Forms.CheckBox();
            this.chkValvulaVaciado = new System.Windows.Forms.CheckBox();
            this.chkTanqueLleno = new System.Windows.Forms.CheckBox();
            this.chkTanqueVacío = new System.Windows.Forms.CheckBox();
            this.PanelImagen.SuspendLayout();
            this.PanelInteriorTanque.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAltura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiámetro)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelImagen
            // 
            this.PanelImagen.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.PanelImagen.BackgroundImage = global::Simulador_de_Tanque_de_Cervecería.Properties.Resources.Recurso_3480;
            this.PanelImagen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PanelImagen.Controls.Add(this.PanelInteriorTanque);
            this.PanelImagen.Controls.Add(this.chkTanqueVacío);
            this.PanelImagen.Controls.Add(this.chkTanqueLleno);
            this.PanelImagen.Location = new System.Drawing.Point(204, 3);
            this.PanelImagen.Name = "PanelImagen";
            this.PanelImagen.Size = new System.Drawing.Size(300, 336);
            this.PanelImagen.TabIndex = 1;
            // 
            // PanelInteriorTanque
            // 
            this.PanelInteriorTanque.BackColor = System.Drawing.Color.Black;
            this.PanelInteriorTanque.Controls.Add(this.panelCerveza);
            this.PanelInteriorTanque.Location = new System.Drawing.Point(166, 104);
            this.PanelInteriorTanque.Name = "PanelInteriorTanque";
            this.PanelInteriorTanque.Size = new System.Drawing.Size(13, 133);
            this.PanelInteriorTanque.TabIndex = 0;
            // 
            // panelCerveza
            // 
            this.panelCerveza.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(142)))), ((int)(((byte)(28)))));
            this.panelCerveza.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelCerveza.Location = new System.Drawing.Point(0, 85);
            this.panelCerveza.Name = "panelCerveza";
            this.panelCerveza.Size = new System.Drawing.Size(13, 48);
            this.panelCerveza.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 201F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 322F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.PanelImagen, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(564, 363);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkValvulaVaciado);
            this.panel1.Controls.Add(this.chkValvulaLlenado);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.btnParo);
            this.panel1.Controls.Add(this.btnVaciar);
            this.panel1.Controls.Add(this.btnLlenar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(195, 357);
            this.panel1.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.97076F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.02924F));
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.nudAltura, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblNivel, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.nudDiámetro, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblVolumen, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblLitros, 1, 4);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 110);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(189, 180);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Arial", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 30);
            this.label5.TabIndex = 2;
            this.label5.Text = "Volumen (l):";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Arial", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Altura (m):";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nudAltura
            // 
            this.nudAltura.DecimalPlaces = 2;
            this.nudAltura.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudAltura.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudAltura.Location = new System.Drawing.Point(106, 3);
            this.nudAltura.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudAltura.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.nudAltura.Name = "nudAltura";
            this.nudAltura.Size = new System.Drawing.Size(80, 21);
            this.nudAltura.TabIndex = 1;
            this.nudAltura.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Arial", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 30);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nivel (m):";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNivel
            // 
            this.lblNivel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNivel.Location = new System.Drawing.Point(106, 30);
            this.lblNivel.Name = "lblNivel";
            this.lblNivel.Size = new System.Drawing.Size(80, 30);
            this.lblNivel.TabIndex = 0;
            this.lblNivel.Text = "0.00";
            this.lblNivel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Arial", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 30);
            this.label4.TabIndex = 0;
            this.label4.Text = "Diametro (m):";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nudDiámetro
            // 
            this.nudDiámetro.DecimalPlaces = 2;
            this.nudDiámetro.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudDiámetro.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudDiámetro.Location = new System.Drawing.Point(106, 63);
            this.nudDiámetro.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudDiámetro.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDiámetro.Name = "nudDiámetro";
            this.nudDiámetro.Size = new System.Drawing.Size(80, 21);
            this.nudDiámetro.TabIndex = 1;
            this.nudDiámetro.Value = new decimal(new int[] {
            12,
            0,
            0,
            65536});
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Arial", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 30);
            this.label3.TabIndex = 0;
            this.label3.Text = "Volumen (m³):";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblVolumen
            // 
            this.lblVolumen.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVolumen.Location = new System.Drawing.Point(106, 90);
            this.lblVolumen.Name = "lblVolumen";
            this.lblVolumen.Size = new System.Drawing.Size(80, 30);
            this.lblVolumen.TabIndex = 0;
            this.lblVolumen.Text = "0.00";
            this.lblVolumen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLitros
            // 
            this.lblLitros.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLitros.Location = new System.Drawing.Point(106, 120);
            this.lblLitros.Name = "lblLitros";
            this.lblLitros.Size = new System.Drawing.Size(71, 30);
            this.lblLitros.TabIndex = 0;
            this.lblLitros.Text = "0.00";
            this.lblLitros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnParo
            // 
            this.btnParo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParo.Location = new System.Drawing.Point(13, 71);
            this.btnParo.Name = "btnParo";
            this.btnParo.Size = new System.Drawing.Size(75, 23);
            this.btnParo.TabIndex = 1;
            this.btnParo.Text = "Paro";
            this.btnParo.UseVisualStyleBackColor = true;
            this.btnParo.Click += new System.EventHandler(this.btnParo_Click);
            // 
            // btnVaciar
            // 
            this.btnVaciar.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVaciar.Location = new System.Drawing.Point(13, 42);
            this.btnVaciar.Name = "btnVaciar";
            this.btnVaciar.Size = new System.Drawing.Size(75, 23);
            this.btnVaciar.TabIndex = 1;
            this.btnVaciar.Text = "Vaciar";
            this.btnVaciar.UseVisualStyleBackColor = true;
            this.btnVaciar.Click += new System.EventHandler(this.btnVaciar_Click);
            // 
            // btnLlenar
            // 
            this.btnLlenar.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLlenar.Location = new System.Drawing.Point(13, 13);
            this.btnLlenar.Name = "btnLlenar";
            this.btnLlenar.Size = new System.Drawing.Size(75, 23);
            this.btnLlenar.TabIndex = 0;
            this.btnLlenar.Text = "Llenar";
            this.btnLlenar.UseVisualStyleBackColor = true;
            this.btnLlenar.Click += new System.EventHandler(this.btnLlenar_Click);
            // 
            // tmrLlenado
            // 
            this.tmrLlenado.Interval = 20;
            this.tmrLlenado.Tick += new System.EventHandler(this.tmrLlenado_Tick);
            // 
            // tmrVaciado
            // 
            this.tmrVaciado.Interval = 20;
            this.tmrVaciado.Tick += new System.EventHandler(this.tmrVaciado_Tick);
            // 
            // tmrEstado
            // 
            this.tmrEstado.Enabled = true;
            this.tmrEstado.Tick += new System.EventHandler(this.tmrEstado_Tick);
            // 
            // chkValvulaLlenado
            // 
            this.chkValvulaLlenado.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkValvulaLlenado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.chkValvulaLlenado.Enabled = false;
            this.chkValvulaLlenado.FlatAppearance.BorderSize = 2;
            this.chkValvulaLlenado.FlatAppearance.CheckedBackColor = System.Drawing.Color.Lime;
            this.chkValvulaLlenado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkValvulaLlenado.Location = new System.Drawing.Point(94, 15);
            this.chkValvulaLlenado.Name = "chkValvulaLlenado";
            this.chkValvulaLlenado.Size = new System.Drawing.Size(33, 19);
            this.chkValvulaLlenado.TabIndex = 4;
            this.chkValvulaLlenado.UseVisualStyleBackColor = false;
            // 
            // chkValvulaVaciado
            // 
            this.chkValvulaVaciado.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkValvulaVaciado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(32)))), ((int)(((byte)(0)))));
            this.chkValvulaVaciado.Enabled = false;
            this.chkValvulaVaciado.FlatAppearance.BorderSize = 2;
            this.chkValvulaVaciado.FlatAppearance.CheckedBackColor = System.Drawing.Color.Lime;
            this.chkValvulaVaciado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkValvulaVaciado.Location = new System.Drawing.Point(94, 44);
            this.chkValvulaVaciado.Name = "chkValvulaVaciado";
            this.chkValvulaVaciado.Size = new System.Drawing.Size(33, 19);
            this.chkValvulaVaciado.TabIndex = 4;
            this.chkValvulaVaciado.UseVisualStyleBackColor = false;
            // 
            // chkTanqueLleno
            // 
            this.chkTanqueLleno.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkTanqueLleno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.chkTanqueLleno.Enabled = false;
            this.chkTanqueLleno.FlatAppearance.BorderSize = 2;
            this.chkTanqueLleno.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.chkTanqueLleno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkTanqueLleno.Location = new System.Drawing.Point(137, 104);
            this.chkTanqueLleno.Name = "chkTanqueLleno";
            this.chkTanqueLleno.Size = new System.Drawing.Size(23, 10);
            this.chkTanqueLleno.TabIndex = 4;
            this.chkTanqueLleno.UseVisualStyleBackColor = false;
            // 
            // chkTanqueVacío
            // 
            this.chkTanqueVacío.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkTanqueVacío.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.chkTanqueVacío.Enabled = false;
            this.chkTanqueVacío.FlatAppearance.BorderSize = 2;
            this.chkTanqueVacío.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.chkTanqueVacío.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkTanqueVacío.Location = new System.Drawing.Point(137, 227);
            this.chkTanqueVacío.Name = "chkTanqueVacío";
            this.chkTanqueVacío.Size = new System.Drawing.Size(23, 10);
            this.chkTanqueVacío.TabIndex = 4;
            this.chkTanqueVacío.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 363);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Simulación de Tanque";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.PanelImagen.ResumeLayout(false);
            this.PanelInteriorTanque.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudAltura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiámetro)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelImagen;
        private System.Windows.Forms.Panel PanelInteriorTanque;
        private System.Windows.Forms.Panel panelCerveza;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnVaciar;
        private System.Windows.Forms.Button btnLlenar;
        private System.Windows.Forms.Timer tmrLlenado;
        private System.Windows.Forms.Timer tmrVaciado;
        private System.Windows.Forms.Button btnParo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudAltura;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNivel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudDiámetro;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblVolumen;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblLitros;
        private System.Windows.Forms.Timer tmrEstado;
        private System.Windows.Forms.CheckBox chkValvulaVaciado;
        private System.Windows.Forms.CheckBox chkValvulaLlenado;
        private System.Windows.Forms.CheckBox chkTanqueVacío;
        private System.Windows.Forms.CheckBox chkTanqueLleno;
    }
}

